import telebot
from config import *
from service import *
import json

tb = telebot.TeleBot(TOKEN)

open_thread = True


@tb.message_handler(commands=['help', 'start'])
def help_list(message):
    tb.send_message(message.chat.id, "* Привет, что бы начать перевод - отправьте мне json файл.")
    tb.send_message(message.chat.id,
                    "* Весь перевод выполняется с русского языка на " + languages.__str__() + " языки.")
    tb.send_message(message.chat.id, "* Перевод может занимать до 2-х минут.")
    tb.send_message(message.chat.id, "* Одновременно доступно выполнение только 1 задачи.")
    tb.send_message(message.chat.id, "* Что бы перевести строку, введите: /t [текст]")
    tb.send_message(message.chat.id, "* Комманды переводов с русского на один язык: ")

    for lang in languages:
        tb.send_message(message.chat.id, "/" + lang + " [текст]")


@tb.message_handler(commands=languages)
def lang_translate(message):
    global open_thread
    if not open_thread:
        tb.send_message(message.chat.id, "Ошибка: поток занят. Попробуйте позже.")
        return

    open_thread = False

    user_message = str(message.text)[4:]

    if len(user_message) <= 1:
        tb.send_message(message.chat.id, "Ошибка: строка должна содержать больше 1 символа.")
        return

    tb.send_message(message.chat.id, "Запрос на перевод принят в обработку.")

    lang = message.text[1:][:2]
    dictionary = dict()
    dictionary[user_message] = user_message

    translate_and_send_string(message, dictionary, lang)
    open_thread = True


@tb.message_handler(commands=['t'])
def translate_message(message):
    global open_thread
    if not open_thread:
        tb.send_message(message.chat.id, "Ошибка: поток занят. Попробуйте позже.")
        return

    open_thread = False

    user_message = str(message.text)[3:]

    if len(user_message) <= 1:
        tb.send_message(message.chat.id, "Ошибка: строка должна содержать больше 1 символа.")
        return

    tb.send_message(message.chat.id, "Запрос на перевод принят в обработку.")

    dictionary = dict()
    dictionary[user_message] = user_message

    for lang in languages:
        translate_and_send_string(message, dictionary, lang)

    open_thread = True


@tb.message_handler(content_types=["document"])
def translate_json(file):
    global open_thread
    if not open_thread:
        tb.send_message(file.chat.id, "Ошибка: поток занят. Попробуйте позже.")
        return

    open_thread = False

    tb.send_message(file.chat.id, "Запрос на перевод принят в обработку.")

    file_id = file.document.file_id
    file_path = tb.get_file(file_id).file_path
    dictionary = json.loads(tb.download_file(file_path))

    for lang in languages:
        translated_dict = do_translate_dict(dictionary, lang)

        if len(translated_dict) > 0:
            sio = to_string_stream(translated_dict, "translations." + lang + ".json")
            tb.send_document(
                file.chat.id,
                sio,
            )
            sio.close()
        else:
            tb.send_message(file.chat.id, "Ошибка перевода на: " + lang.upper())

    open_thread = True


def translate_and_send_string(message, dictionary, lang):
    translated_dict = do_translate_dict(dictionary, lang)
    if len(translated_dict) > 0:
        tb.send_message(
            message.chat.id,
            json.dumps(translated_dict, ensure_ascii=False).replace('{', '').replace('}', '')
        )
    else:
        tb.send_message(
            message.chat.id,
            'Ошибка перевода на: ' + lang
        )


tb.infinity_polling()
