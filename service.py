import time
import json
import googletrans
from six.moves import StringIO
from config import count_translate_try, google_service_urls


def to_string_stream(dictionary: dict, name: str) -> StringIO:
    dumps = json.dumps(dictionary, ensure_ascii=False)

    sio = StringIO(dumps)
    sio.name = name
    return sio


def do_translate_dict(input_dict: dict, dest: str, src='ru', count_try=0) -> dict:
    if count_try > count_translate_try:
        return dict()

    dictionary = input_dict
    keys = list(dictionary.keys())

    count_try += 1

    try:
        translator = googletrans.Translator(service_urls=google_service_urls)

        list_items = translator.translate(keys, dest, src)
        for translation in list_items:
            if dictionary[translation.origin][0].isupper():
                dictionary[translation.origin] = translation.text[0].upper() + translation.text[1:]
            else:
                dictionary[translation.origin] = translation.text
        return dictionary
    except AttributeError as ex:
        print(ex)
        time.sleep(5)
        return do_translate_dict(input_dict, dest, src, count_try)
    except Exception as ex:
        print(ex)
        return dict()
